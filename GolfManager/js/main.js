'use strict';

$(document).ready(function(){
  // Activate Carousel
  $("#carousel").carousel({
    interval: 2000,
    wrap: true,
    pause: 'hover'
  });

  // Enable Carousel Indicators
  $('#indicators').on('click', 'li', function() {
    $('#myCarousel').carousel($(this).index());
  });
});