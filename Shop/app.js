let express      = require('express'),
    path         = require('path'),
    favicon      = require('serve-favicon'),
    cookieParser = require('cookie-parser'),
    bodyParser   = require('body-parser'),
    _            = require('underscore'),
    mongo        = require('mongodb'),
    monk         = require('monk'),
    db           = monk('localhost:27017/shop');

let app = express();
let routes = require('./routes/index');

// All environments
app.set("port", 80);
app.engine('ejs', require('express-ejs-extend')); // express-ejs-extend
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Make our db accessible to our router
app.use(function(req,res,next){
  req.db = db;
  next();
});

// Local variables
app.locals._ = _;


// Load the routes
app.use('/', routes);
app.use('/subCats/:topCatId/:subCatId', routes);
app.use('/catProds/:subCatId', routes);

app.listen(app.get("port"), function() {
  console.log("Express server listening on port " + app.get("port"));
});

module.exports = app;