let express = require('express');
let app     = express();
let router  = express.Router();
let soap    = require('soap');

// This code is executed for every request to the router
router.use(function(req, res, next) {

  let catColl = req.db.collection('categories');
  catColl.find({}, function (err, topCats) {
    app.locals.topCats = topCats;

    next();
  });

});


/* Main page */
router.get('/', function (req, res) {
  res.render('index', {
    title   : 'Home page',
    topCats : app.locals.topCats
  });
})

/* GET Subcategories page */
router.get('/subCats/:subCatId', function (req, res) {
  let mdbClient = require('mongodb').MongoClient;
  let dburl = 'mongodb://localhost:27017/shop';

  console.log('Finding subcategories...');

  let subCatId = req.params.subCatId;
  let rootCatId = subCatId.split('-')[0];

  console.log('rootCat: ' + subCatId.split('-')[0]);
  console.log('subCatId: ' + subCatId);

  mdbClient.connect(dburl, function (err, db) {
    if (err) { throw err; }

    let catsColl = db.collection('categories');
    catsColl.aggregate([
      { $match: { "id": rootCatId } },
      { $unwind: {path: "$categories"}},

      { $match: {"categories.id" : subCatId } },
    ]).toArray(function (err, subCats) {

      res.render('index', {
        "title"   : 'Subcategory page',
        "topCats" : app.locals.topCats,
        "subCats" : subCats
      });

    });

  });

});

// GET Products in category page
router.get('/catProds/:subCatId', function (req, res) {
  let mdbClient = require('mongodb').MongoClient;
  let dburl = 'mongodb://localhost:27017/shop';

  console.log('Finding products...');

  let subCatId = req.params.subCatId;
  console.log('subCatId: ' + subCatId);

  mdbClient.connect(dburl, function (err, db) {
    if (err) { throw err; }

    let prodsColl = db.collection('products');
    prodsColl.find({'primary_category_id' : subCatId}).toArray(function (err, products) {

        res.render('index', {
          title    : 'Subcategory page',
          topCats  : app.locals.topCats,
          subCatId : subCatId,
          products : products
        });

    });
  });
});


/* GET Product description page */
router.get('/showProduct/:subCatId/:productId', function (req, res) {
  let mdbClient = require('mongodb').MongoClient;
  let dburl = 'mongodb://localhost:27017/shop';
  let wsdlUrl = 'http://infovalutar.ro/curs.asmx?wsdl';

  console.log('Show product description page ...');

  let productId = req.params.productId;
  let subCatId = req.params.subCatId;

  console.log('productId: ' + productId);

  mdbClient.connect(dburl, function (err, db) {
    if (err) { throw err; }

    let prodsColl = db.collection('products');
    prodsColl.find({'id' : productId})
      .toArray(function (err, product) {

      res.render('productsCategory', {
                title    : 'Subcategory page',
                topCats  : app.locals.topCats,
                subCatId : subCatId,
                product  : product,
              });

    });
  });
});

let baseToLeu;

router.get('/currencies', function (req, res) {
  console.log('########');
  console.log('Get converted currency...');

  let wsdlUrl = 'http://infovalutar.ro/curs.asmx?wsdl';

  let previous = req.query.previous;
  let next = req.query.next;

  if (previous === 'USD') {
    baseToLeu = 3.8821;
  }

  app.locals.fromCurrency = previous;
  app.locals.toCurrency = next;

  soap.createClient(wsdlUrl, function (err, soapClient) {
    if (err){
      return res.status(500).json(err);
    }

    soapClient.GetLatestValue({
      Moneda : next
    }, function (err, result) {
      if (err){
        return res.status(500).json(err);
      }

      let response = result.GetLatestValueResult;
      console.log('response: ' + response);

      console.log(next + ' to leu = ' + response);
      let calc = (baseToLeu / response).toFixed(2);

      console.log('1 ' + previous + ' = ' + calc + ' ' + next) ;
      baseToLeu = response;

      res.json(calc);

    });

  });

});


module.exports = router;